# Terraform guide for AWS common EC2+RDS stack

## Welcome

This guide will take you through a journey. In this journey you will see how to create a traditional stack on Amazon Web Services (AWS) using Infrastructure-as-code principles.
By the end, you will have a full high-availability secure environment with multiple servers and a relational database. 
 
This guide is designed to be easy to follow, and I did my best to avoid big leaps between two running states of our stack. 
Any time a new key element is introduced, we'll see why and how it fits in the whole schema. 

This is a hands-on journey. Although you can just read it, I strongly recommend executing the code for the examples yourself. I can’t stress this enough: you’ll retain a lot more if you change the code and actually run it.  
At every subset, you will therefore run your own infrastructure, play with it, and make sure you understood what happened and you’re following along. 

## Who should read this guide?

The intent is to introduce people to infrastructure as code. We assume people interested by this topic have some familiarity with the underlying infrastructure elements.
Therefore we assume a few things:
- you have some entry-level knowledge of AWS. You might not be a savvy user, but you know what that an EC2 instance is a machine provisioned for you on which you can run your software.
- you should know how to use git. All the incremental steps are git commits, and if you change things (the region, key name, ...), knowing how to use git stash will be helpful to save you some time.
- you know the basics of shell. We won't do fancy stuff here, but we'll use `curl`, `ssh`. If you never ever used this, it's just a very little extra step you'll have to do to be able to follow along.
- you are not already familiar with infrastructure as code or with Terraform, but ou want to ramp-up on this. If you are already familiar with these, I will probably waste your time (but will appreciate your feedback).


## Why should you read this guide?

Why should we embrace infrastructure as code? is it a new shiny and trendy thing? 
One of the most appealing reason for you, if you have to run and manage your servers, is that descriptive and immutable infrastructure makes it easy (at least easier) to restore your infrastructure stack in a well-known healthy state.
In other words, one of the main benefits is the **reduction of your MTTR** (Mean Time To Recover from an outage). If you ever fear that some server crashes and you have to reinstall every packages with the correct version, even if you have a hand-written notebook. Fear no more!

To achieve this, we'll write **descriptive** code. This is really important: the code should not define what are the actions to perform. It must define the desired state in a descriptive fashion. Then the tools will resolve the actions to go from the current state to that desired state.

There are many other advantages with this approach: since this is code, you will version it with your usual tools (e.g. Git), you will be able to perform code-review before to apply changes to your infrastructure. Which means all the usual benefits of code-review will ow apply to your infrastructure: early bug/issue detection, knowledge sharing, challenging choices, spreading the knowledge within the team, ...

There's also a cost optimization aspect: since all your resource will be managed from the code, you won't have _ghost_ servers any longer. You know, those old Vms started by some previous team member for a previous project that nobody uses any longer, everybody forgot about it, but it still cost some money. And indeed, although you'll probably go into the web UI from time to time for the convenience of looking for something, you will no longer have to browse all these various menus and sections to find the right thing.

Here, we are going to focus on Terraform as the tool, and AWS as the environment. Mainly because these are the most popular choices as of today, and most of the knowledge you'll gain here can then be applied with other tools or other clouds.

Terraform is not the only one, you could use AWS CloudFormation, Google Cloud Deployment Manager Templates, Azure Resource Manager Templates or OpenStack Heat. But Terraform works with all these clouds (and other ones, like Oracle if you have to).
Furthermore, Terraform is open-sourced and developed by HashiCorp, which you might already know from their other tools like Vagrant, Nomad, Consul, Vault or Packer. Finally, if you haven't read this before, I encourage you to read [The Tao of HashiCorp](https://www.hashicorp.com/tao-of-hashicorp). How could someone not love a company with such a clear and awesome vision?!

## How to use this guide?

Each milestone is divided in steps. And each step has its own commit. 
In each commit, the associated README is updated (e.g. README-step08.md is updated multiple times to explain what is going one in each commit). 
Start by reading the README update. 
This will tell you what we are trying to achieve, and will give you the high-level view of the changes and the reasons behind the changes.
But do have a look at the complete diff too for every commit. Those diffs are never large, and since some changes are obvious, I do not mention every single one of them in the README to focus of the relevant parts, but by actively going through the diffs you will make sure you get what is happening.

## Tips and tricks

Terraform has some special names and suffixes. One of them is `_override.tf`. If you have files named `override.tf` or `whatever_override.tf`, the resources/data defined in those files will be merged (and will overwrite) the content of the other files.
This can be useful to keep the sample file intact, but customize it in your own override. (That way you don't have to git stash it for every time you checkout any update) 
